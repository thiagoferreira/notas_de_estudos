**O que é Web3?**
<details><summary>Click to expand</summary>
Introdução <br>
Web3 é sobre a próxima evolução da web.
Uma web mais aberta, transparente, descentralizada e executada em infraestrutura de blockchains.
É uma mudança de paradigma em direção a uma Internet mais democratizada, ou seja, uma internet que é governada pelo coletivo e não por corporações e interesses especiais.

**Web 1:**
<details><summary>Click to expand</summary>
    - Era "somente leitura" da internet.<br>
    - Aproximadamente entre os anos 1980 e o início dos anos 2000, foi quando a maior parte da internet consistia em sites estáticos. Os usuários da Internet lêem conteúdo ou informações on-line sem interagir ou gerar conteúdo por conta própria.<br>
</details>

**Web2:**
<details><summary>Click to expand</summary>
    - A Web 2 é a era de "leitura/gravação", ou seja, nosso momento atual e que estamosfamiliarizados.<br>
    - Os usuários da Internet não apenas consomem conteúdo on-line, mas também geram muito dele - redes sociais como Instagram, Twitter, YouTube e TikTok são plataformas fornecidas por conteúdo gerado pelo usuário.<br>
    - Os dados são "gravados" nessas plataformas em bancos de dados centralizados que possuem dados de usuários e podem fazer o que quiserem com eles, independentemente dos desejos dos usuários incluindo vendê-los ou derrubar contas e conteúdos por qualquer motivo que desejarem.<br>
</details>

**Web3:**
<details><summary>Click to expand</summary>
    - Web3 é a era do "ler/escrever/propriedade" que está surgindo.<br>
    - Utiliza a tecnologia blockchain para permitir que os usuários possuam ativos digitais e seus dados.<br>
    - A Web3 também representa um retorno aos protocolos de código aberto que não podem ser alterados ou manipulados de acordo com as regras de empresas centralizadas como Google e Apple.<br>

> "Blockchains são computadores que podem assumir compromissos. Na web3, os usuários são capazes de criar conteúdo e construir aplicativos
> em cima de protocolos blockchain sem medo de serem "puxados pelo tapete" por mudanças arbitrárias".
</details>

**WEB3 consite em:**
<details><summary>Click to expand</summary>
• Blockchain<br>
• Criptomoedas<br>
• Tokens<br>
• DeFi (Decentralized Finance)<br>
• DAO (Decentralized Autonomous Organization)<br>
• NFT (Non Fungible Token)<br>
• Metaverso<br>
</details>

</details>

**Economia de Propriedade**
<details><summary>Click to expand</summary>
    - Usuários tem total controle/responsabilidade com seus ativos, assim como a moeda feduciaria em que você tem nas mãos/carteira.<br>
    - Os ativos da sua carteira está na blockchain e não na propriedade de um banco/instituição financeira.<br>
    - A confiança está no código e não em terceiros como Bancos para transicionar dinheiro e Advogados para redigir um contrato.<br>
    - Descentralização do poder:
Sempre houve desconfiança no poder concentrado.
Seja o governo, os bancos centrais ou as corporações
globais de tecnologia que se tornaram mais poderosas
do que as nações.<br>
    - Censura: Uma entrevista extremante censurada na China, no início da pandemia do novo coronavírus, é o destaque. Enquanto foi totalmente banida de redes sociais, algumas pessoas subiram o material para a blockchain da Ethereum.
Dessa forma, o texto da repórter Sarah Zheng foi colocado na rede Ethereum, não sendo permitido ao governo chinês exluir o conteúdo.<br>
    - De fato, a blockchain é um banco de dados mundial e imutável, que não permite alterações em um conteúdo gravado.

**Propriedade do valor:**
<details><summary>Click to expand</summary>
    - As empresas de tecnologia modernas hospedam conteúdo de
usuários e, em seguida, monetizam de forma privada.<br>
    - São explorados nosso desejo de interação social e os
combinam com padrões de produtos viciantes, exibem anúncios e extraem todo esse valor gerado pelo.<br>
    - Se o serviço é de graça, você é o produto.<br>
    - A idea da Web3 é remover esses intermediarios, vender/compartilhar dados com quem interessar e ganhar com isso diretamente. Ex: Mirror no lugar do Medium.
</details>
</details>

**Por que a Web3 Importa?**
<details><summary>Click to expand</summary>
A Web3 é importante porque estamos construindo uma internet de propriedade de pessoas e removendo intermediários.
É a democratização do acesso e a distribuição de poder e controle.
Resistência à censura.
</details>