**Blockchain:**
<details><summary>Click to expand</summary>
	- Banco de dados distribuído com registros de todas as transações que foram executados e compartilhados entre as partes participantes.<br>
    - Todos os computadores da rede possuem uma cópia idêntica do livro de transações.<br>
    - Um dos usos famosos do Blockchain é o Bitcoin. O Bitcoin usa prova criptográfica em vez de confiança de terceiros para que duas partes executem transações pela Internet.<br>
    - Nós/Nodes: Um computador conectado a rede. Quanto mais nodes existe mais a rede fica descentralizada. Validador de transações.<br>
    - Pilares da blockchain:
    <details><summary>Click to expand</summary>
        • Descentralização: não há uma autoridade central para validação dos dados;<br>
        • Imutabilidade dos dados: cada nó que compõe a rede possui sua cópia dos dados, fornecendo redundância;<br>
        • Segurança: garantida pela criptografia;<br>
        • Registros distribuídos: a rede blockchain é composta de vários nós que compartilham o poder computacional;<br>
        • Consenso: a validação é feita por meio de algoritmos de consenso que comparam os dados dos nós.<br>
     </details> 
    - Benefícios da tecnologia Blockchain:
    <details><summary>Click to expand</summary>
    - Economia de tempo: Nenhuma verificação de autoridade central necessária para liquidações, tornando o processo mais rápido e barato.<br>
    - Economia de custos: uma rede Blockchain reduz as despesas de várias maneiras. Não há necessidade de verificação de terceiros. Os participantes podem compartilhar ativos diretamente. Os intermediários são reduzidos.<br>
     - Segurança mais rígida: ninguém pode alterar os dados da Blockchain, pois é compartilhado entre milhões de participantes. O sistema é seguro contra crimes cibernéticos e fraudes.
    </details>    
</details>

**Bitcoin:**
<details><summary>Click to expand</summary>
    - Full-nodes:<br>
        - Responsável por retransmitir as novas transações e blocos.<br>
        - Um full node faz download de uma cópia da blockchain do Bitcoin contendo todo histórico da transação.<br>
        - Todas as transações estão nos full-nodes desde a criação do bitcoin.<br>
    - Mining-node:<br>
        - Responsável por agrupar as transações para criar um bloco candidato à mineração.<br>
        - Isso acontece após aprovação do Full-Node.<br>
        - Se conseguir resolver o problema matemático, ele consegue uma recompensa em bitcoin.<br>
        - Volta pro Full-Node e adiciona o bloco na blockchain.<br>
    - Light-Node:<br>
        - Responsável por armazenar o histórico do que já foi aprovado pelo full-node, é um auxiliar.><br>
    - Super-Node:<br>
        - Tem a mesma função do Full-node, no entanto, com maior capacidade de verificar e aprovar as transações.<br>
</details>  

**Ethereum:**
<details><summary>Click to expand</summary>
    - Smart Contract:
    <details><summary>Click to expand</summary>
        - São simplesmente programas(script) armazenados em um blockchain que são executados quando condições predeterminadas são atendidas. Ao invés de você conectar no banco de dados conecte na blockchain!<br>
        - Criar e implantar aplicativos e serviços: Instrumentos financeiros, jogos, rastreabilidade, NFT, aplicações em geral.<br>
        - Qualquer desenvolvedor pode criar um contrato inteligente,tendo que pagar a taxa.
    </details>
    - EVM:
    <details><summary>Click to expand</summary>
        - Compilador da Ethereum.
        - Há um único computador canônico (chamado de Ethereum Virtual Machine, ou EVM).<br>
        - Todos que participam da rede Ethereum (cada nó Ethereum) mantêm uma cópia do estado deste computador.<br>
        - Código é escrito em alto nivel em Solidity e depois é compilado para bytecodes com através da EVM.
    </details>
    - Porcesso de verificação de Transação:
    <details><summary>Click to expand</summary>
        - Transação criada e propagada para rede - > Verificação do nonce da transação: Se Nonce estiver incorreto transação é descartada, se estiver correto segue com as validações.<br>
        - Em seguida é verificada a assinatura digiral, se a assinatura estiver incorreta a transação é descartada, se estiver correta segue com as validações.<br>
        - Depois é verificado o double-Speding, se for detectado o double-Speding a transação é descartada, se não for detectado a transação é validada.
    </details>
    - Transações Internas são transações geradas pelo próprio contrato para a manipulação do mesmo.
</details>

**Blockchain Trilema:**
<details><summary>Click to expand</summary>
    - As Blockchains tem os seguintes trilemas: Segurança, Escalabilidade, Descentralização.<br>
    - Nenhuma atual blockchain consegue ser excelente em todos esses pontos.<br>
    - Bitcoin e Ethereum tem segurança e descentralização mas não tem escalabilidade, Ripple e Smart Chain tem segurança e escalabilidade mas não são descentralizadas.<br>
    - Nenhuma blockchain atingiu o trilemas ainda, mas ainda estão em busca disto.
</details>

**Blockchain e Criptomoedas:**
<details><summary>Click to expand</summary>
    - Criptomoedas e tokens são coisas diferentes. Criptomoeda = Moeda nativa de um blockchain(Ex: BTC, ETH, ADA). Token = Criado dentro de uma blockchain existente(REVV, PORTO).<br>
    - Supply = Número de ofertas disponível para negociação.<br>
    - Altcoins são definidas como todas as criptomoedas que não sejam o Bitcoin.<br>
    - Status Legal
    <details><summary>Click to expand</summary>
        - Como as criptomoedas não emitidas por nenhuma entidade pública ou privada cada jurisdições financeiras em todo o mundo tem suas regras. Exemplos:<br>
        • China é proibido.<br>
        • Japão propriedade legal.<br>
        • El Salvador e República Centro-Africana consideram como moeda de curso legal.<br>
        • União Europeia estão dentro da lei.<br>
        • EUA não é titulo financeiro.<br>
        • Brasil é legal e está sendo criado regras para proteger os investidores.
        </details>
</details>

**Smart Contract:**
<details><summary>Click to expand</summary>
    - Como exemplo vamos pensar em uma vending machine, que funciona de maneira semelhante a um contrato inteligente - entradas específicas garantem saídas predeterminadas.<br>
    - Um tipo de conta dentro da blockchain, ou seja, têm saldo e podem enviar transações pela rede.<br>
    - Não são controlados por um usuário, em vez disso, são implantados na rede e executados conforme programado, igual o case da vending machine.<br>
    - As contas de usuário podem interagir com um contrato inteligente enviando transações que executam uma função definida no contrato inteligente.<br>
    - Contratos inteligentes podem definir regras, como um contrato regular, e aplicá-las automaticamente por meio do código.<br>
    - Os contratos inteligentes não podem ser excluídos por padrão e as modificações com eles são irreversíveis.<br>
    - Um contrato pode chamar outro contrato.<br>
    - Blockchains que suportam contratos inteligentes:<br>
    <details><summary>Click to expand</summary>
        - Ethereum<br>
        - Solana<br>
        - Polkadot<br>
        - Cardano<br>
        - Avalanche<br>
        - Cosmos<br>
        - Algorand<br>
        - Elrond<br>
        - Ergo<br>
        - Tron<br>
    </details>
    - O que podemos criar como smart contracts?
    <details><summary>Click to expand</summary>
        • Tokens<br>
        • Stablecoins<br>
        • Armazenamento de dados<br>
        • Atividades comerciais<br>
        • Supply chain<br>
        • Empréstimos<br>
        • Seguros<br>
        • Contrato de prestação de serviço<br>
        • Direitos Autorais<br>
        Processo de votação<br>
        • Saude<br>
        • NFTS, DAOs, Metaversos, Games, Finanças, etc, etc...<br>
        - dApps (Aplicação Descentralizadas)
    </details>
    - Possiveis Vulnerabilidades:
    <details><summary>Click to expand</summary>
        • Falhas na codificação<br>
        • Informação sensíveis expostas<br>
        • Controle de acesso<br>
        • Reentrancy<br>
        • DoS
    </details>
</details>

**Consenso:**
<details><summary>Click to expand</summary>
    - CONSENSO é um conceito que descreve um tipo de acordo produzido por consentimento entre todos os membros de um grupo. Se estabele quando duas ou mais partes chegam a um ponto comum de decisão..<br>
    - Na blockchain o conseso se dá com todos os nodes aprovando aquela transação, modificação, etc.<br>
    - O consenso é necessário em vários níveis do Bitcoin:
    <details><summary>Click to expand</summary>
        - Como por exemplo a manutenção do código-fonte do Bitcoin e deve ser mantido entre todos os nó que armazenam e validam o blockchain. No nível do código-fonte, o consenso é alcançado ao permitir que qualquer pessoa proponha, revise e comente as mudanças.<br>
        - Esse processo geralmente é mais lento do que os projetos centralizados porque a discussão e a revisão são aplicadas intensivamente a qualquer mudança antes de sua implementação.<br>
        - No entanto, esse processo garante que nenhum interesse especial seja atendido em detrimento de outros, e nenhuma parte privada é capaz de ditar o futuro do Bitcoin para seu próprio ganho.<br>
        - No nível do blockchain, o consenso deve ser mantido por todos os nós que executam cópias compatível.
    </details>
</details>

**Proof of Work:**
<details><summary>Click to expand</summary>
    - É um método para afirmar a validade dos dados.<br>
    - Força os mineradores a gastar grandes quantidades de energia e dinheiro na produção de blocos, incentivando-os a permanecerem honestos, protegendo assim a rede.<br>
    - É uma das únicas maneiras pelas quais uma rede descentra pode concordar com uma única fonte de verdade.<br>
    - Para adicionar informações, chamadas de blocos, ao blockchain, um membro da rede deve publicar uma prova de que investiu um
trabalho considerável na criação do bloco. Este trabalho impõe grandes custos ao criador e, portanto, os incentiva a publicar informações honestas.<br>
    - Além disso, uma vez que um bloco foi adicionado ao blockchain, é extremamente difícil ou impossível removê-lo, tornando o passado do Bitcoin imutável.<br>
    - Cada bloco tem um Header que contem o hash do bloco anterior, merkle root hash e Nonce que é o resultado do puzzle do bloco e a lista de transações.<br>
	- Os blocos estao conectados uns com os outros pela informacao do header que tem o hash do bloco anterior.<br>
    - Se algum membro da rede tentar transmitir informações falsas, todos os nós da rede irão imediatamente reconhecê-las como inválidas e ignorá-las.<br>
    - Conceito de registros de transações na blockchain:
    <details><summary>Click to expand</summary>
        - 1. Passo o registro da transação: De forma simplificada vamos pensar no blockchain como uma planilha do excel, entretanto, a blockchain é descentralizada e distribuída. Sendo assim qualquer pessoa pode ter uma cópia dessa planilha em seu computador.Ex: De: UserA / Para: UserB / Valor: 10 <br>
        - 2. Sigilo e Privacidade: Através de criptografia, ninguém sabe quem são as pessoas envolvidas na transação. Cada endereço (address) na blockchain é criptografado à partir de uma chave privada em sua posse. Você precisa gerar uma chave pública para a pessoa que vai te enviar a quantia. Ex: De: Oxdb....e8 / Para: Oxcd....43 / Valor: 10 <br>
        - 3. Executando a transação: Quando A faz uma transação e envia criptomoeda para B, essa transação não é automaticamente incluída no blockchain. Ela precisa ser validada. Por isso, ela fica em uma espécie de área temporária, esperando ser incluída no blockchain. Ex: De: Oxdb....e8 / Para: Oxcd....43 / Valor: 10 / Status: Pendente <br>
        - 4. Analisando as transações: A cada 10 minutos (Dependendo da Blockchain) todas as transações do mundo que estão na área temporária são analisadas por uma pessoa, chamada de miner, ou seja, um minerador. Essa "pessoa", é um computador ligado à rede do blockchain que, após ganhar uma competição matemática (algoritmos de criptografia), ganha o direito de incluir as transações
pendentes no blockchain. Para isso, ele ganha um prêmio, em Bitcoins. Quando A fez a transação para B, ele assinou digitalmente as informações dessa transação com sua chave privada. Esse processo de assinatura é como uma operação criptográfica em
cima de todas as informações da transação e, igualmente, gera uma chave representada por
um novo conjunto de caracteres e números (a assinatura). Ex: De: Oxdb....e8 / Para: Oxcd....43 / Valor: 10 / Assinatura: 0x6b8...c22<br>
        - 5. Transação executada: O minerador vai confirmar que essa assinatura é válida e nesse momento é oficializada a data da transação, através de um carimbo oficial de tempo. Por fim, o próprio minerador assina a transação e gera novos procedimentos criptográficos, dando origem, para cada transação, ao "transaction id", a chave que oficializa a transação.Ex: De: Oxdb....e8 / Para: Oxcd....43 / Valor: 10 / Assinatura: 0x6b8...c22 / Status: OK / Data-Hora: 2024-08-18 19:25 / transaction Id: 3p5a...Kgu<br>
    </details>
    - Analogia do problema matemático encontrado na mineração dos blocos: Uma boa analogia são aqueles cadeados com sequências de números. Não existe uma fórmula matemática
que, caso resolvida, irá abrir o cadeado. O único modo de conseguir abrí-lo é descobrir a sequência de números, e para isso a única possibilidade é tentar e tentar (0000, 0001, 0002, e assim por diante). Uma vez descoberta, ela é facilmente verificada por outras pessoas, assim como também na rede.
    - Critica há Proof of Work: <br>
    - Uma dura crítica e importante à prova de trabalho é a quantidade de energia necessária para manter a rede segura e a descentralizada. Quanta energia a mineração consome? O Índice de Consumo de Energia Bitcoin do Digiconomist estimou que uma transação de bitcoin leva 1.449 kWh para ser concluída, ou o equivalente a aproximadamente 50 dias de energia para uma família média dos EUA.
Para colocar isso em termos monetários, o custo médio por kWh nos EUA está próximo de 12 centavos. Isso significa que uma transação de
bitcoin geraria aproximadamente uma conta de energia de US$ 173. A mineração de Bitcoin usa tanta energia quanto a Argentina, de
acordo com o Índice de Consumo de Energia Bitcoin, e nesse nível anualizado de 131,26 terawatts-hora, a mineração de criptomoedas estaria entre os 30 principais países com base no consumo de energia. O consumo de energia para mineração de bitcoin atingiu seu maior
nível no final de 2021 e nos primeiros meses de 2022, consumindo mais de 200 terawatts-hora.  <br> 
</details>

**Proof of Stake:**
<details><summary>Click to expand</summary>
    - A prova de participação reduz a quantidade de trabalho computacional necessário para verificar blocos e transações. A prova de participação altera a forma como os blocos são verificados usando as máquinas dos proprietários de moedas. Os proprietários oferecem suas moedas como garantia para a chance de validar blocos. Os proprietários de moedas com moedas "travadas" (stake) tornam-se "validadores".<br>
    - Os validadores são selecionados aleatoriamente para "minerar" ou validar o bloco (forjar). Esse sistema escolhe quem pode "minerar" em vez de usar um mecanismo baseado em competição, como prova de trabalho.<br>
    - Quanto mais tokens e mais tempo maior a recompensa.<br>
    - Os blocos são validados por mais de um validador, e quando um número específico de validadores verifica se o bloco está correto, ele é finalizado e fechado.<br>
    - Os validadores oficiais(32 ETH), caso seja dectado que houve tentativa de hacker/manioulação são removidos esses 32 ETH do usuário.<br>
    - Uma transação é criada e enviada para um node que inclui essa transação em um bloco. O node faz a validação do bloco, fazendo a validação dos dados, depois que esta tudo correto esse bloco é adicionado a propria chain do node e propagado para os outros nodes em que ele esta conectado. Os outros nodes fazem a validação desse node também para garantir que esta tudo certo.
    - Os Nós recebem Shard chains(data layer) ao invés da blockchain inteira, que é basicamente um pedaço apenas da blockchain.
    - São 64 Shard chains existentes na Ethereum, e para haver coordenação entre eles existe a Beacon Chain(coordination chain), que faz basicamente é pegar dados de um shard chains e fazer disponivel para outro grupo de shards.
    - São necessários 128 Nodes para validar um bloco, e esse grupo de Nodes é chamado de Comitê.
    - O comitê tem o time frame de 12s para propor e validar um shar block, esse time frame é conhecido por slot.
    - Slot é a chance do block ser adiconado tanto na beacon chain como na shard chain.
    - Um épico é um conjunto de 32 slots.
    - Apenas um bloco valido é criado por slot.
    - Depois de ter sido realizado um épico o comitê é enviado para a pool e enbaralhado randomicamente e criaod um novo comitê, isso para ajudar os shards se manter seguros de agentes maliciosos.   
</details>

**Gas Fee:**
<details><summary>Click to expand</summary>
    - É a taxa necessária para realizar uma transação ou executar um contrato inteligente na blockchain. <br>
    - As taxas são precificadas em pequenas frações do éter de criptomoeda (ETH). O gás é usado para pagar aos validadores os recursos necessários para realizar as transações.O preço exato do gás é determinado pela oferta, demanda e capacidade da rede no momento da transação.<br>
    - Gas, Gwei e Limit:
    <details><summary>Click to expand</summary>
        - Gas: é o valor cobrado cada vez que uma transação ou contrato inteligente são executados no Ethereum;<br>
        - Gwei: É a unidade de medida na qual o gas é contabilizado;<br>
        - Gas limit: é a quantidade máxima que uma transação exige para ser executada na rede.<br>
    </details>
    - cost (ETH) = gasLimit x gasPrice x (1/10(elevado a 9))<br>
Exemplo - Maria envia 2ETH para João, ela escolheu pagar
2 Gwei:
21000 × 2 × (1/10(elevado a 9)) = 0.000042 ETH
</details>

**Fork:**
<details><summary>Click to expand</summary>
    - "Em engenharia de software, uma bifurcação ou ramificação (em inglês: fork) acontece quando um desenvolvedor (ou um grupo de desenvolvedores) inicia um projeto independente com base no código de um projeto já existente, ou seja, quando um software é desenvolvido com base em outro, já existente, sem a descontinuidade deste último."<br>
    - Quando a comunidade identifica um bug ou melhoria no código é iniciado a discussão para ver se vai haver um soft ou hard fork.<br>
    - SOFT FORK: Chegamos em um consenso, todos concordaram e vamos atualizar a aplicação, são coisas mais tranquilas mais operacionais.<br>
    - HARD FORK: Quando normalmente os desenvolvedores tem atualizações em um nível de programação mais densa e para fazer essa atualização você bifurcar a rede. Uma blockchain se transforma em duas sequências.<br>
    - No Soft fork a atualização é aplicada em alguns nodes,validads e depois aplicada em todos os nodes, a blockchain se mantem a mesma.<br>
    - No hard fork é criada uma nova blockchain a partir da outra, e as duas segues em cadeias diferentes e tokens diferentes(Ex: Bitcoin e Bitcoin Cash).<br>
    - Razões para a ocorrência de um fork: • Adicionar nova funcionalidade; • Corrigir problemas de segurança; • Transações reversas.
</details>

**Layers:**
<details><summary>Click to expand</summary>
    - As layers (camadas) estão surgindo para contornar o grande trilema das blockchains.<br>
    - LO - Network(Infra)<br>
    - L1 - Plataforma Blockchain(BTC, ETH, SOL)<br>
    - L2 - Protocolos(L2 processa diversas transações e finaliza na L1), aumentando a escalabilidade.<br>
    - L3 - Aplicações que utilizam a L2, Ex: Uniswap, Quickswap, PancakeSwap, DeFi.<br>
    - L4 - Aplicações como Brave, Metamask...Dapps.<br>
    - As camadas ainda são soluções em desenvolvimento. Escalabilidade é um gargalo ainda para adoção em massa.
</details>

**Rollups:**
<details><summary>Click to expand</summary>
    - São protocolos de camada 2 (Layer 2) na blockchain da Ethereum que ajudam a processar transações separadamente da rede principal para aumentar a velocidade (escala) e reduzir os custos de transações (taxa).<br>
    - São juntado algumas transações na L2 até formar um bloco e esse bloco é enviado para a L1 como uma unica transação. <br>
    - Tipos de Rollups: 1- Optimisct Rollups, 2- Zero-Knowledge  Rollups (ZK-Rollups).<br>
    - Optimistic Rollups: O rollup "otimista", foi chamado assim pois seu mecanismo pressupõe que todas as transações enviadas à blockchain são válidas, embora nunca tenha sido verificado se as transações foram realizadas corretamente. As transações passam por um período de "teste" no qual qualquer um pode duvidar da validade desse tipo de transação e fazer uma contestação.Consenso: Prova de Fraude.<br>
    - ZK-Rollups: O rollup de "conhecimento zero" permite que alguém prove matematicamente que uma afirmação é verdadeira sem divulgar informações adicionais sobre essa afirmação como se fosse um documento, exemplo passaporte.A escala é mais rápida.Consenso: Prova de Validade.
</details>

**Privacy Coin:**
<details><summary>Click to expand</summary>
    - Uma criptomoeda de privacidade usa a tecnologia blockchain para dificultar a vinculação de um indivíduo a uma transação, fornecendo anonimato às partes envolvidas e confidencialidade de detalhes da transação, como o valor. Ao contrário da crença popular, a maioria das criptomoedas, incluindo Bitcoin, NÃO fornecem anonimato. Como parte de seu design, as transações registradas em uma blockchain são públicas.<br>
    - Há pessoas na comunidade criptográfica que acham que o design original do blockchain de Satoshi fornece muita transparência, isso porque ao utilizar as CEXs é necessario KYC, então as moedas de privacidade foram desenvolvidas para corrigir essa "falha".<br>
    - As moedas de privacidade normalmente fornecem privacidade de duas maneiras: 1. Anonimato para ocultar as identidades das pessoas
por trás das transações; 2. Não rastreabilidade para evitar que observadores externos sigam uma trilha de transação.<br>
    - As moedas de privacidade usam vários métodos para tornar seus usuários anônimos e suas transações não rastreáveis, como usar endereços "escondidos" únicos e "misturar" várias transações para criar uma única transação.<br>
    - Privacy Coins mais populares atualmente: XMR, ZEC, DASH.<br>
    - Legalidade: Alguns países percebem as moedas de privacidade como uma saída para lavagem de dinheiro ou financiamento do terrorismo e as baniram completamente, enquanto outros as deixaram em uma área legal cinzenta. Por exemplo, nos EUA, as moedas de privacidade são legai Mas na Coreia do Sul e no Japão, negociar (e manter) moedas de privacidade é ilegal.<br>
</details>

**Exploradores de Bloco:**
<details><summary>Click to expand</summary>
    - Um explorador de blocos é um mecanismo que permite pesquisar uma determinada informação no blockchain.<br>
    - As atividades realizadas em blockchains são conhecidas como transações, que ocorrem quando as criptomoedas são enviadas de e para endereços de carteira.<br>
    - Cada Blockchain tem seu explorador de blocos.<br>
</details>

**Tipos de Blockchains:**
<details><summary>Click to expand</summary>
    - PÚBLICA:
    <details><summary>Click to expand</summary>
        - • Totalmente transparente e aberta;<br>
>       - Não há restrições de entrada, qualquer pessoa pode participar das validações das transações;<br>
        - • Descentralizada;<br>
        - • Incentivo para participar: Criptomoeda.<br>
        - Exemplos: BTC, ETH, SOL, ADA.
    </details>
    - PRIVADA:
    <details><summary>Click to expand</summary> 
        - • Semelhante a uma rede pública de blockchain.<br>
        - > Descentralizada, no entanto, centralizada; =)<br>
        - • Administrada por uma organização;<br>
        - • Controle de quem tem permissão para participar;<br>
        - • Pode ser executada por trás de um firewall corporativo ou hospedada on-premises.<br>
        - Exemplos: reciChain - Basf
    </details>
    - HÍBRIDA:
    <details><summary>Click to expand</summary> 
        - • Permite que as organizações configurem um sistema privado baseado em permissão junto com um sistema público sem permissão, permitindo o controle de quem pode acessar dados específicos armazenados no blockchain e quais dados serão abertos ao público.<br>
        - • Elementos de blockchain público e privado;<br>
        - • KYC (Know Your Client);<br>
        - • Não permite usuários anônimos.<br>
        - Exemplos: Quorum, Ripple.
    </details>
    - CONSÓRCIO:
    <details><summary>Click to expand</summary> 
        - • Diversas empresas colaboram em uma rede descentralizada;<br>
        - • Semelhança com blockchain privada;<br>
        - • Transparência nas transações e poder de controle;<br>
        - • É ideal para as empresas quando todos os participantes precisam ser autorizados e ter responsabilidade compartilhada.<br>
        - Exempo: Corda R3, Aura.
    </details>
    - Importância para as empresas:
    <details><summary>Click to expand</summary>
        - • Informações imediatas e precisas.<br>
        - • Transparência.<br>
        - > Armazenamento de forma imutável.<br>
        - > Confiança.<br>
        - > Contratos Inteligentes.
    </details>
</details>

**Double-Speding:**
<details><summary>Click to expand</summary>
    - É a tentativa de gastar o mesmo btc duas vezes, enviar o mesmo btc para dois endereços diferentes por exemplo.<br>
    - Isso é evitado pela rede, durante a mineração dos blocos é verificado se aquele btc já foi gasto uma outra vez, sendo assim se ouver dois blocos contendo o envio do mesmo btc, quando um deles for validado e entrar na rede o outro bloco que tem a transação do mesmo btc se torna invalido pois é detectado que aquele btc já foi utilizado.<br>
    - Cada nó da rede Bitcoin valida todas as transações recebidas antes de aceitá-las em sua mempool. Parte dessa validação envolve checar se os Bitcoins que você está tentando gastar ainda estão disponíveis para gastar (não foram gastos em outra transação).<br>
    - Eles verificam a entrada da transação, que contém a referência para a transação anterior de onde os Bitcoins foram recebidos (o output anterior). Cada entrada em uma transação deve apontar para um "UTXO" (Unspent Transaction Output) — um saldo de Bitcoin não gasto.<br>
    - Uma vez que um bloco é minerado e incluído na blockchain, a transação que está nesse bloco é considerada confirmada.<br>
    - Os outros nós da rede aceitam esse bloco como parte do histórico imutável da blockchain. Qualquer tentativa de incluir a segunda transação em um bloco subsequente será automaticamente rejeitada pela rede, pois a rede já tem um consenso de que os Bitcoins foram gastos na primeira transação. Com isso, o gasto duplo é evitado de forma eficiente.<br>
</details>

**Defi:**
<details><summary>Click to expand</summary>
 
 - Defi é uma instituição financeira descentralizada.
 - Com as DeFi é possível realizar basicamente tudo o que um banco convenciona faz, como:
    - • obter juros;
    - • tomar e conceder empréstimos;
    - • comprar seguros;
    - • negociar derivativos.
</details>

**Dapp's:**
<details><summary>Click to expand</summary>

 - DApps são aplicativos de código aberto que usam a linguagem Solidity em uma blockchain, onde armazenam e recuperam dados fazendo interface com a tecnologia blockchain.
 - Características dos Dapp's:
    - • O aplicativo deve ser completamente open-source e
autônomo;
    - • Deve armazenar os dados em blockchain;
    - • Deve utilizar um token como meio de recompensa;
    - • Deve gerar tokens.
</details>

**Visão Geral dos Tokens:**
<details><summary>Click to expand</summary>

 - Fungibilidade:
    - Fungivel: O dinheiro é o bem fungível por excelência, dado que quando se empresta uma quantia a alguém (por exemplo, R$100,00), não se está exigindo de volta aquelas mesmas cédulas, mas sim um valor, que pode ser pago com quaisquer notas de Real (moeda). Ex: Criptomoedas.
    - Não Fungível: Imagine que uma fabricante de bolsa de luxo faz um único modelo de uma bolsa. Esse item, por ser o único no mundo, não pode ser substituído por outros da mesma espécie; logo, ele não é fungível.Ex: NFTs.
 - Tipos de Tokens:
    - Utility Tokens: Os tokens utilitários representam o acesso a um determinado produto ou serviço, geralmente em uma rede blockchain
específica. Ex: sistema da quermesse onde você compraas fichas para poder ir consumindo as coisas.
    - Security Tokens: Representam ativos financeiros. Por exemplo, uma empresa pode emitir ações tokenizadas, concedendo ao titular direitos de propriedade e dividendos. Do ponto de vista legal, seriam idênticas às ações tradicionalmente distribuídas. Ex: Ações tokenizadas.
    - Stablecoins: Paridade 1 para 1. Ex: USDT, USDC = 1 dólar.
</details>

**Token Economy:**
<details><summary>Click to expand</summary>

 - Ao analisar/criar um projeto devemos levantar e investigar as seguintes questões sobre o token Economy:
    - • 1. Quais tokens o projeto usa?
    - • 2. Se houver vários tokens, por quê?
    - • 3. Qual é o valor de mercado do token?
    - • 4. Qual é o suprimento circulante (Supply)?
    - • 5. Qual é a oferta máxima possível?
    - • 6. Qual a utilidade desse token?
    - • 7. Vai haver demanda desse token no mercado?
 - Valor intrínseco do token:
    - O valor intrínseco de um Token é definido pela finalidade a que serve. O token deve TER USO DENTRO DO PRÓPRIO ECOSISTEMA.
    - Exemplo o ETH. Toda transação na rede exige que os usuários paguem taxas de gás em $ETH.
 - Token Supply:
    - O fornecimento do token afeta seu valor. Quanto mais escasso for um token, maior será o seu valor.
    - No entanto, a escassez não torna um token valioso por si só. Outros fatores como oferta circulante, oferta máxima, valor de mercado e valor totalmente diluído são algumas das métricas importantes que você deve verificar.
    - Exemplo: Haverá apenas 21 milhões de Bitcoins, e ninguém pode criar mais Bitcoins. • O fornecimento de Bitcoin é limitado;
• A demanda está aumentando; • O preço deve aumentar.
 - Mecanismos inflacionários e deflacionários:
    - A inflação em cripto é tecnicamente semelhante à inflação fiduciária. A inflação ocorre se a oferta circulante de tokens aumenta e vice-versa para a deflação. Muitos projetos dependem de emissões de token para recompensar seus usuários.
 - Alocação e Distribuição:
    - A alocação de tokens ajuda a entender quem detém a quantidade máxima de tokens. Isso pode ajudá-lo a evitar uma queda de preço.
    - Se Venture Capital e grandes investidores detiverem um grande número de tokens, há grandes chances de eles descartarem o token no seu lançamento.
    - Temos também o Vesting significa quando eles estão autorizados a vender os tokens. Deixar o token travado por um tempo.
 - Conclusão e Aprendizado:
    - Está pensando em começar um projeto Web3? Comece pelo Token Economy!. Veja o token economy através das lentes da oferta, demanda e
incentivos de longo prazo. O Token Economy é importantíssimo, no entanto, narrativas e hype podem superar o Token Economy e isso não é bom.Acredite ou não, muitos projetos têm Token Economy horríveis mas uma equipe de marketing excelente, mas isso não se sustenta ao longo prazo.
</details>

**Stablecoin:**
<details><summary>Click to expand</summary>

 - Stablecoins: Paridade 1 para 1. Ex: USDT, USDC = 1 dólar.
 - Tipos de Stablecoins:
    - • Stablecoins com garantia fiduciária
    - • Stablecoins cripto-colateralizadas
    - • Stablecoin Algorítmicas
 - Stablecoins com garantia fiduciária:
    - Mantêm reserva de uma moeda fiduciária (ou moedas), como o dólar americano, como garantia. Essas reservas são mantidas por custodiantes independentes e são auditadas regularmente. Exemplo: USDT (Tether) e USDC são stablecoins populares lastreadas em reservas em dólares americanos.
 - Stablecoins cripto-colateralizadas:
    - Utiliza um colateral/lastro descentralizado, o que pode ser a solução para o problema em relação à confiança no emissor.
    - São lastreadas em criptomoedas descentralizadas como o Ether que possuem alta volatilidade e são geridas por smart contracts para manter sua estabilidade. Um exemplo é a stablecoin DAl da MakerDAO.
 - Stablecoin Algorítmicas:
    - Este modelo de stablecoin não possui lastro. O que mantém o preço da moeda digital estável são algoritmos que controlam a quantidade de ativos em circulação, fazendo operações de queima ou emissão de tokens conforme a necessidade para que o preço se mantenha
sempre o mesmo.
    - Problemas: O preço da stablecoin algorítmica TerraUSD (UST) caiu mais de 60% em 11 de maio de 2022, vaporizando sua atrelagem ao dólar americano, já que o preço do token Luna usado para atrelar a stablecoin caiu mais de 80%.
</details>

**CBDC:**
<details><summary>Click to expand</summary>

 - CBDC (Central Bank Digital Currency) é a moeda digital emitida por bancos centrais, é uma moeda virtual de um país.
 - A única forma de muitos bancos centrais emitirem dinheiro é por meio de notas e moedas em espécie. Com uma CBDC, também será possível emitir moedas no formato virtual, colocando em circulação moedas que nunca foram impressas.
</details>

**ICO, IEO, IDO, IFO, IAO:**
<details><summary>Click to expand</summary> 

 - Uma Initial Public Offering (IPO), em português, oferta pública inicial refere-se ao processo de oferta de ações de uma empresa privada ao público em uma emissão de ações pela primeira vez. Um IPO permite que uma empresa levante capital de investidores públicos. As empresas devem atender aos requisitos do regulador local para realizar um IPO.
 - ICO - Initial Coin Offering:
    - Ofertas iniciais de moedas se tornaram uma maneira popular de arrecadar fundos para produtos e serviços geralmente relacionados a criptomoedas.
    - Para participar de uma ICO, você geralmente precisa primeiro comprar uma moeda digital mais estabelecida, além de ter um  entendimento básico de carteiras e trocas de criptomoedas.
    - Algumas ICOs renderam retornos para os investidores. Muitos outros se revelaram fraudulentos ou tiveram um desempenho ruim.
    - As ICOs são, em sua maioria, completamente não regulamentadas, portanto,os investidores devem ter um alto grau de cautela e diligência ao pesquisar e investir nelas.
 - (STO) Security Token Offering / Ofertas de tokens securitizados
(IEO) Initial Exchange Offering / Ofertas iniciais em exchange
(IDO) Initial DEX Offerings / Ofertas iniciais de DEX
(IFO) Initial Farm Offering / Oferta Inicial de Fazenda
(IAO) Initial Airdrop Offering / Oferta Inicial de Airdrop
(IGO) Initial Game Offering / Oferta Inicial de Jogo
(FTO®) Fan Token Offering / Oferta de Fan Tokens
</details>

**DAO:**
<details><summary>Click to expand</summary>

 - Dao: Estabelecer uma empresa ou uma organização que possa funcionar absolutamente sem gerenciamento hierárquico. Seus membros compartilham um objetivo comum de agir no melhor interesse da entidade.
 - Nas empresas tradicionais, quando uma decisão precisa ser tomada, os executivos são os responsáveis por essa decisão. Em um DAO esse processo é democratizado, e qualquer membro pode criar uma proposta, e todos os outros membro podem votar nela. Cada proposta criada tem um prazo para votação, e após o prazo a decisão é favorável ao resultado da votação (SIM ou
NÃO).
 - Como é organizado:
    - • A proposta são publicadas publicamente em fóruns quepermitem a votação.
    - • O processo de votação para DAOs é publicado em uma blockchain de forma transparente.
    - • O poder de voto geralmente é distribuído entre os usuários com base no número de tokens que eles possuem(varia de DAO para DAO). Isso para que o usuário coloque em risco seus tokens e não prejudique o restante da comunidade.
    - • Após a decisão o código é executado de forma autônoma.
 - Tipos de DAOs:
    <details><summary>Click to expand</summary>
        - • Protocol DAOs<br>
        - • Grant DAOs: Para doações<br>
        - > Social DAOs: Pessoas com ideias semelhantes, artistas por exemplo.<br>
        - • Collector DAOs: Para comprar NFTs blue-chip e outros ativos.<br>
        - • Venture DAOs: Geral pool para fazer investimentos.<br>
        - > Philanthropy DAO<br>
        - > Media DAOs<br>
        - > Social Media DAOs<br>
        - • Entertainment DAOs<br>
        - ETC<br>
    </details>
 - Protocol DAOs:
    - Os DAOs de protocolo são uma das opções mais comuns e populares entre os DAOs.
    - Geralmente trabalham com um token de governança de votação para implementar as modificações necessárias. Ex: Uniswap
</details>