Clean code:

 **Chapter 1: Clean Code**
<details><summary>Click to expand</summary>
	- Custo de um código ruim.<br>
	- Um código limpo deve ser elegante, eficiente, legivel, simples, sem duplicações e bem escrito.<br>
	- O código deve agregar valor ao negócio.<br>
	- Um código limpo oferece qualidade e compreensão.<br>
	- Um código ruim reduz a produtividade e rendimento.<br>
	- Não são só técnicas, é uma filosofia.<br>
	- Os principios e técnicas são simples.<br>
	- Bater o olho no código e conseguir compreender com o menor esforço possivel.<br>
	- O código limpo faz bem apenas uma coisa.<br>
</details>

	
 **Chapter 2: Nomes significativos**
 <details><summary>Click to expand</summary>
	- Use nomes que revelem seu propósito.<br>
	- Use nomes pronunciaveis.<br>
	- Use nomes passiveis de buscas.<br>
	- Nomes de classes devem ser substantivos.<br>
	- Nome de metodos devem ser verbos.<br>
</details>

**Chapter 3: Funções**
 <details><summary>Click to expand</summary>
	- Funções pequenas.<br>
	- Função deve fazer apenas uma coisa.<br>
	- Blocos condicionais curtos.<br>
	- Um nivel de abastração por função.<br>
	- Regra decrescente: codigo precisa ser lido de cima pra baixo, não começar o entendimento pelo o meio(ex).<br>
	- Use nomes descritivos.<br>
	- Parametros em funções: Não deve se utilizar muitos, se esta utilizando muito pode ser indicativo que a função esta fazendo mais que uma coisa, nos casos que precisa de muitos parametros pode se considerar passar um objeto.<br>
	- Separação comando-consulta: Uma função deve sempre apenas fazer ou responder algo, nunca deve fazer ambos.<br>
	- Tratamento de exceções.<br>
	- DRY: Don't repeat your self.<br>
</details>
	
**Chapter 4: Comentários**
 <details><summary>Click to expand</summary>
	- Só fazer comentarios onde o codigo nao possa ficar de uma forma legivel, por exemplo um regex complexo.<br>
	- Alerta sobre consequencias, por exemplo deixar um cometario do porque um testes estar desbailitado por exemplo por ele demorar muito tempo para ser executado.<br>
	- TODO: coisas que precisam ser feitas.<br>
</details>

**Chapter 5: Formatação**
<details><summary>Click to expand</summary>
	- Não ter arquivos muito grande, máximo de 200 linhas e no máximo do máximo 500.<br>
	- Criar o codigo em forma vertical, a parte do código mais abastrata no inicio e as funções de mais baixo nivel no fim.<br>
	- Espaçamento entre conceitos(idéias).<br>
	- Continuidade e Distancia verticais:<br>
		- Declarações das variáveis de uma classe devem estar seguidas linha a linha.<br>
		- Variaveis locais devem ser declaradas o mais proximo possivel do seu uso.<br>
		- Funcões que sao dependentes devem também ficar próximas verticalmente.<br>
		- Funções com afinidade conceitual também devem ficar próximas.<br>
	- Ordenação Vertical: Sempre que possivel coloque a função que esta sendo chamada embaixo da função que esta chamando.<br>
	- Ordenação Horizontal, não fazer linhas cumpridas, maixmo pra não fazer rolagem na tela do notebook.<br>
	- Parenteses junto ao norme da função e espaço entre os parametros.<br>
	- Identação.<br>
</details>
	
**Chapter 6: Objetos e Estrutura de Dados**
<details><summary>Click to expand</summary>
	- Os atributos da classe devem ser privados, sendo possivel apenas instaciar  no construtor ou por outros metodos da classe que validem os dados.<br>
	- Não deve ser possivel alterar os atributos da classe diretamente.<br>
	- Usar objeto de dados quando tiver pouco comportamento e muitos dados e usar objetos para o inverso.<br>
	- Lei de Dmeter, objetos devem limitar suas interações com os amigos próximos.<br>
		- Cada unidade deve ter conhecimento limitado sobre outras unidades apenas unidades se relacionam.<br>
	- DTO, transferencia de dados entre cmadas do software, o objetivo é agrupar os dados a serem enviados, ex enviar dados para o DB.<br>
	- Não ter muitas chamadas juntos(trem)<br>
</details>

**Chapter 7: 	Tratamento de Erro**
<details><summary>Click to expand</summary>
	- Usar try catch<br>
	- Criar mensagens informativas e passar juntamente a exceção.<br>
	- Não retorne null.<br>
	- Evitar passar null e tratar valores nulos.<br>
	- Os erros devem ter o contexto de onde o erro ocorreu.<br>
	- Deve haver separação da logica de negócio e de tratmento de erros.<br>
</details>
	
**Chapter 8: Limites**
<details><summary>Click to expand</summary>
	- Uso de biblioteca de terceiros com moderação, evitar usar muitas bibliotecas.<br>
	- Tentar desenvolver o codigo que esta usando de terceiros(se possivel).<br>
</details>	

**Chapter 9: Teste Unitário**
<details><summary>Click to expand</summary>
	- Ter teste limpos e objetivos.<br>
	- Teste limpo = legibilidade.<br>
	- Teste deve ter um unico objetivo.<br>
		- Testes devem ser rapidos.
		- Testes devem ser independentes um dos outros.
		- Teste tem que ser executavel em qualquer ambiente.
		- Os testes tem que ter um assert/ uma validação.
		- Thorough, validar vários tipos de cenários.
</details>
		
**Chapter 10: Classes**
<details><summary>Click to expand</summary>
	- Classe menor possivel.<br>
	- Ter varias classes, cada uma com sua partição.<br>
	- Na Classe vem primeiros as variaveis, construtores e depois as funções.<br>
	- Organizar para alterar, fazer organizações em bloco para alterar, não sair alterando tudo de uma vez.<br>
</details>
	
**Chapter 11: Sistemas**
<details><summary>Click to expand</summary>
	- Construir sistemas que sejam escalaveis e de facil manutenção.<br>
	- Criar sistema em módulos.<br>
	- Main não deve ter muitas chamadas na inicialização do sistema.<br>
	- Utilizar injeção de dependencias(spring boot).<br>
	- Um sistema nunca nasce 100% de primeira, mas criado seguindo as boas praticas e padrões do mercado é mais facil fazer a progressão/refactorar esse sistema.<br>
	- Devemos seguir padões mas principalmente padrõs que resolvam o problema do cliente, não adiantar ficar fazendo overthink nos padrões se nao estiver pensando na solução do problema ser atingida.<br>
</details>

**Chapter 12: Sistemas**
<details><summary>Click to expand</summary>
	- SOLID<br>
	- Sistema deve ser passivel de testes.<br>
	- Refatoração.<br>
	- Evitar repetição de código.<br>
	- Código deve expressar claramente a intenção do autor.<br>
	- Projeto Simples ><br>
		- Efetuar todos os testes.<br>
		- Sem duplicação de código.<br>
		- Expressar o proósito do programador.<br>
		- Minimizar o número de classes e métodos.<br>
</details>		
		
**Chapter 13: Concorrência**
<details><summary>Click to expand</summary>
	- Nem sempre a concorrência melhora o desempenho.<br>
	- Apenas um metodo com synchronized por objeto.<br>
	- Testar o código com Threads.<br>
	- Tipos de Problemas em Threads:<br>
		- Bound Resources.<br>
			- Producer - Consumer: uma produz e a outra lê, uma fica esperando o sinal da outra.<br>
		- Mutual Exclusion.<br>
		- Starvation.<br>
			- Readers - Writers: enquanto uma esta escrevendo a outra não pode ler, enquanto uma esta lendo a outra não pode escrever, a taxa de transferencia pode ser um problema e gerar esoera indefinida(starvation). 
		- Deadlock.<br>
		- Livelock.<br>
</details>

**Chapter 14: Refatoramento**
<details><summary>Click to expand</summary>
	- Conseguir ler o código de cima pra baixo.<br>
	- Não existe código perfeito de primeira, crie primeiro, faça funcionar e depois começe a aplicar o clean code.<br>
	- Modificações excessivas podem arruinar o código, melhor fazer as alterações em bloco e ir aplicando os testes para garantir que está ok.<br>
	- Ficar satisfeito apenas do código estar executando, é importante revisar o código para melhorias.<br>
</details>

**Chapter 15: Junit**
<details><summary>Click to expand</summary>
	- Junit é um framework que facilita o desenvolvimento e execução de testes em códido Java.<br>
	- Por que usar Testes Unitários? <br>
		- Previne contra 0 aparecimento de "BUG'S" oriundos de códigos mal escritos;<br>
 		- Código testado é mais confiável;<br>
 		- Permite alterações sem medo (coragem);<br>
		- Testa situações de sucesso e de falha;<br>
 		- Resulta em outras práticas ХР como: Código coletivo, refatoração, integração contínua;<br>
		- Serve como métrica do projeto (teste ==requisitos);<br>
 		- Gera e preserva um "conhecimento" sobre as regras de negócios do projeto.<br>
</details>

**Chapter 16: Refatorando Serial Date**
<details><summary>Click to expand</summary>
	- Antes de refatorar execute o código que você vai mexer para garantir que ele está funcionando.<br>
	- Analisar one pode ser refatorado.<br>
	- Refatorar.<br>
</details>