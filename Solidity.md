**Métodos no Solidity:**

   - Modificadores podem ser utilizaos para:
    <details><summary>Click to expand</summary>
        • Restrição de acesso<br>
        • Validação de entradas<br>
        • Proteção contra hack de reentrada<br>
        - Caso a condição do modificador seja satisfeita ao chamar uma função, a função será executada e, caso contrário, uma exceção será lançada.<br>
        ```solidty
        contract Owner {
            modifier onlyOwner {
                require(msg. sender == owner);
                _;
            }
            function abort() onlyOwner { // Uso do Modificador
            // ...
            }
        }
        ``` 
   </details>

**Funções View:**
<details><summary>Click to expand</summary>

 - As funções de visualização (view) são funções somente leitura, o que garante que as variáveis de estado não possam ser
modificadas após chamá-las.    
 - Se as instruções que modificam variáveis de estado, o compilador lança um warning em tais casos. aviso Por padrão, um método get é a função view.
 ```
 //variáveis de estado
uint num1 = 10;
uint num2 = 16;
function getResult() public view returns(uint product, uint sum){
product = num1 * num2;
sum = numi + num2;
}
```
</details>

**Funções Pure:**

<details><summary>Click to expand</summary>
 - As funções puras (pure) não leem nem modificam as variáveis de estado, retornando os valores apenas utilizando os parâmetros passados para a função ou variáveis locais presentes nela.

  ```
 function getResult() public pure returns (uint product, uint sum)
 {
     uint num1 = 2;
     uint num2 = 4;
     product = num1 * num2;
     sum = num1 + num2;
 }
```
</details>

**Library:**

<details><summary>Click to expand</summary>

 - Para usar uma biblioteca no código do contrato devemos fazer o import dela no padrão JS, import LibraryName from 'filePath'.
 - Depois deve ser informado para qual tipo de dado aquela biblioteca vai ser vinculada, < libraryName> for < dataType>.
```
// Definição do contrato
contract LibraryExample {
    // Deploy da biblioteca para o tipo de dado inteiro
    using LibExample for uint;
    address owner = address(this);
    // Chamando a função da biblioteca
    function getPow(uint num1, uint num2) public view       returns (uint, address) {
        return num1. pow (num2);
    }
}
```
</details>

**Variaveis Storage e Memory:**
<details><summary>Click to expand</summary>

 - Memory no Solidity se refere a um armazenamento temporário enquando Storage trata de preservar os dados
entre chamadas de funções.
 - Storage grava os dados na blockchain copm isso tem um custo de gas maior.
 - Após o contrato ser executado os dados na memory são apagados mas os que estão storage se mantem, podendo ser acessados novamente já que estão armazendados na blockchain.
 - É recomendado utilizar Memory para cálculos intermediários e o Storage para o armazenamento final do
resultado.
 - Variáveis de estado(globais), variaveis locais, structs e arrays são sempre armazenadas como Storage;
 - Argumentos de funções são sempre armazenadas como
Memory;
</details>

**Padrões de contratos inteligentes:**
<details><summary>Click to expand</summary>

 - Tokens são Smarts Contracts também.
 - Os padrões de tokens são definitos pela ERC - Ethereum Request for Comment que definie a convenção para os smart contracts, com regras para interação com os contratos.
 - ERC-20: Fungible token standard that provides basic functionality to transfer tokens, as well as allow tokens to be approved.
 - Erc-721: Non-Fungible Token standard.(NFT)
 - ERC-777: Standard that defines all the functions required to send tokens on behalf of another address, contract or regular
account. Evolução do ERC-20.
 - ERC-1155: A standard for contracts that manage multiple token types.
</details>


**O padrão ERC 20:**
<details><summary>Click to expand</summary>

 - O padrão ERC-20 define um conjunto de regras que devem ser atendidas para que um token seja aceito e capaz de interagir com outros tokens na rede.
 - Um token ERC-20 deve ser obrigatoriamente:
• Fungível;
• Transferível;
• Base monetária fixada.
 - O padrão ERC-20 possui Getters, Funções(realizar transaçãoes) e Eventos que
definem o comportamento do token.
 - Getters:
 ```
 function totalSupply() external view returns (uint256)
//Retorna a quantidade de tokens existentes.
function balanceOf(address account) external view returns (uint256);
// Retorna a quantidade de tokens pertencentes a um endereço
function allowance(address owner, address spender) external view returns (uint256);
// 0 padrão ERC-20 permite que um endereço autorize outro endereço a recuperar tokens dele.
``` 
 - Funções:
 ```
 function transferFrom(address sender, address recipient, uint256 amount) external returns
(bool);
//Move uma quantidade de tokens entre endereços e deduz do saldo do emissor. Retorna um
evento Transfer
```
 - Eventos(Avisos):
```
event Transfer(address indexed from, address indexed to, uint256 value);
//Evento emitido quando a quantidade de tokens é enviada de um endereço para outro
event Approval(address indexed owner, address indexed spender, uint256 value);
//Evento emitido quando uma quantidade de tokens é aprovada pelo dono do contrato para ser
enviado por um spender.
```
- 
</details> 

**Tokens ERC 20:**
<details><summary>Click to expand</summary>

 - Embora a criptomoeda Ether possa ser minerada, os tokens do baseados em Ethereum não podem.
 - Ao criar um contrato, é determinado o fornecimento total de unidades do token (total suppy)e o cronograma de distribuição. Quando um novo token é criado, ele é cunhado, do inglês minted.
</details> 