Introducao a Blockchain:

**Binary, decimal and hexadecimal numbers:**
<details><summary>Click to expand</summary>
	- Numeros binarios sao 0 e 1.
</details>

 **Random Numbers:**
<details><summary>Click to expand</summary>
	- Numeros gerados aleatoriamente. <br>
	- Computador nao consegue gerar um numero aleatorio do nada, entao realiza PNRG(Pseudo number Random generator).<br>
	- PNRG usa um numero como input e depois faz uma operacao matematica que vai gerar o random number.<br>
	- Numero de inout geralmente é um numero pequeno e o random number é geralmente um numero grande.<br>
	- Na Blockchain os random numbers sao usados por exemplo para gerar as chaves publicas e privadas.<br>
	[Youtube Video](https://www.youtube.com/watch?v=lEDQNZCwxus&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=2)
</details>

 **Hash:**
<details><summary>Click to expand</summary>
	- Hash fucntion transforma qualquer data e de qualquer tamanho em uma String de datamanho determinado.<br>
	- Exemplo: "Meu texto" = PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x <br>
	- Uma pequena mudança no dado vai fazer com o que o Hash mude, por exemplo trocar o M maiusculo por minusculo ja vai fazer com o que o Hash seja diferente.<br>
	- Quando dois diferentes dados geram o mesmo Hash isso é chamado de Hash collision. Ex: "Meu texto" e um myMusic.mp3 geram o mesmo hash.<br>
	- Uma hash fucntion é considerada "collision resistant" quando é muito dificil dois dados trazarem o mesmo hash output.<br>
	- Exemplos de hash fucntions: md5, sha256, keccak-256 <br>
	- Esse recurso das funções hash é amplamente utilizado na segurança da informação, principalmente na proteção de senhas. Quando você registra uma conta em um site, sua senha é transformada por uma função hash, resultando em um resumo de hash que é então armazenado pelo serviço. Ao fazer login, a senha inserida passa pela mesma função hash, e o hash resultante é comparado ao armazenado para verificar sua identidade. <br>
	- Por serem bastante eficientes, rápidas e até mesmo exclusivas, as funções dos algoritmos hash são muito utilizadas pela tecnologia blockchain para registros, incluindo a verificação de transações realizadas. <br>
	- Fora isso, as funções hash também são usadas para diferentes aplicações dos contratos inteligentes — que são contratos digitais programáveis e autoexecutáveis —, a fim de criar novos endereços de criptomoedas e, também, para o processo de mineração.<br>
	[Youtube Video](https://www.youtube.com/watch?v=UQlQrYmQauU&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=3)
	[Artigo sobre o tema](https://academy.bit2me.com/pt/o-que-%C3%A9-hash/)
</details>

 **Cryptography, encrypt, decrypt:**
<details><summary>Click to expand</summary>
	- Encryption convert an readable message in an unreadble message. Ex: "Meu Texto" = "@#B!sbs"<br>
	- Decryption convert an unreadable message in a readble message.<br> Ex: "@#B!sbs" = "Meu Texto"<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=4)
</details>

 **Symmetric keys and asymmetric keys:**
<details><summary>Click to expand</summary>
	- On Symmetric Encryption user A encrypty the file and a key is genetared, so user A send this ecrypted file to user B, that only can open it with the key genetared by user A, the problem with this approach is that user A needs to send the key to user B to him/her can open it, so if hacker intercepts that key he/she is able to open the file<br>
	- Asymmetric Encryption (RSA Algorithm) users have public and private keys.<br>
		- User A encrypts file with public key from user B.<br>
		- File can only be opened with private key from user B.<br>
		- Even user A can't open the ecrypted file because this only decrypt with user B private key, so even if the hacker is able to get the file he/she isn't able to open it.  <br>
		- Public key is used to encrypt and private is used to decrypt.
	- Asymmetric Encryption is used on HTTPS protocol, SSH, Blockchain.
	- On Asymmetric Encryption with private key is possible to know the public key but the opposite isn't possible.
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=5) <br>
	[Youtube Video](https://www.youtube.com/watch?v=AQDCe585Lnc)
</details>

 **Digital signature:**
<details><summary>Click to expand</summary>
	- Uma assinatura digital é semelhante ao uma assinatura por escrito, mas uma assinatura por escrito pode ser facilmente copiada.<br>
	- Uma assinatura digital prove para o destinatario as seguintes informações:<br>
		- A menssagem foi criada por um remetente conhecido (autenticacao).<br>
		- O remetente nao pode negar a mensagem que foi enviada(non-repudiation). <br>
		- A menssagem nao foi alterada no transito do envio(integridade).<br>
	- Como assinatura digital é criada:
		- User A faz o hash do dado(documento, video, texto, etc).<br>
		- User A usa sua chave privada e o hash gerado e encripta isso, gerando um hash.<br>
		- O hash encriptado é chamado de assinatura digital.<br>
	- Como assinatura digital é validada:
		- O user B quer um documento do user A, user A manda o documento e a assinatura digital.<br>
		- User B desincripta a assinatura digital e a public key do user A, o resultado é um hash(has A). <br>
		- User B aplica o mesmo hash algoritmo no documento, gerando outro hash(has B).<br>
		- User B compara os dois hash para ver se sao iguais.<br>
		- Se foram iguais comprova que o documento nao foi alterado durante o envio e que pertece ao user A.<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=6) <br>
</details>

 **Diffie-Hellman key exchange:**
<details><summary>Click to expand</summary>
	- Diffie-Hellman key exchange é um método para assegurar a troca de uma senha compartilhada entre entidades/pessoas através de um canal publico.<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=10) 
</details>

**Elliptic Curve keys(Gerando chaves publicas e privadas):**
<details><summary>Click to expand</summary>
	- Elliptic Curve Key generation em Blockchains como Bitcoin e Ethereum é usado para gerar public e private key. <br>
	- A principal vantagem de usar Elliptic Curve Key é a diminuição do tamanho da key e consequentemente a velocidade para gerar a chave. <br>
	- Elliptic Curve Key utiliza um tamanho de chave menor em comparacao a RSA e tendo praticamente o mesmo nivel de seguranca.<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=11)
</details>

**ASCII:**
<details><summary>Click to expand</summary>
	- ASCII é muito usado para conversão de Código Binário para Letras do alfabeto maiúsculas ou minúsculas.<br>
	- Vai de 0 a 127.<br>
	- Por exemplo a palavra Cat convertido para ASCII fica: 67,97,116 <br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=12)
</details>

**Base-64:**
<details><summary>Click to expand</summary>
	- ABase-64 é basicamente uma maneira de codificar dados arbitrários em texto ACII.<br>
	- Esquemas de codificação Base-64 são comumente usados ​​quando há necessidade de codificar dados binários (imagens, áudio) que precisam ser armazenados e transferidos por mídias projetadas para lidar com dados textuais. Isso para garantir que os dados permaneçam intactos sem modificação durante o transporte.<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=13)
</details>

**Checksum:**
<details><summary>Click to expand</summary>
	- Alguns numeros de identificacao como os numeros das contas bancarias tem alguns digitos incorporado a esses numeros. Esses digitos que podem ser caracteres ou numeros sao chamados de checksum digits e sao usados para verificar algum erro de digitacao.<br>
	- O checksum tambem é utilizado na blockchain do bitcoin par checar os endereços.<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=15)
</details>

**Convert public, private key pairs to addresses:**
<details><summary>Click to expand</summary>
	- Um numero de NIS ou numero da conta do banco tem apenas como objetivo identificar o usuario.<br>
	- Uma chave publica tem uma chave privada correspondente e que estao matematicamente linkadas. Essas chaves sao usadas para criar transacoes em as partes e envio de dados de forma criptografada.<br>
	- As randomicas chaves privadas e publicas sao transformadas em enderecos publicos e privados.<br>
	<details><summary>- Algumas das razoes em transformar as chaves em enderecos:</summary>
		- Implementar o checksum para verificar se ha algum error de digitacao.<br>
		- Implementar o numero da versao no endereco para diferenciar implementacoes de Bitcoins parecidas(Bitcoin, litecoin).<br>
		- Aplicar o base-58 enconding.<br>
		- Aplicar o algoritmo Hash para diminuir o tamanho do endereço.
		</details>
		[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=16)
</details>

**Bitcoin compressed and uncompressed addresses:**
<details><summary>Click to expand</summary>
	- Uma chave privada é um numero randomico que é usada para gerar a chave publica que é um conjunto de dois inteiros(x e y).<br> 
	- Compressed key utiliza menos espaco na rede, desta forma tendo taxa de gas mais barato.<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=17)
</details>

**Peer-to-peer network:**
<details><summary>Click to expand</summary>
	- Peer-to-peer/Decentralized network sao um grupo de computadores chamados nodes que estao interligados para compartilhar dados um com os outros sem ter um computador centralizado.<br> 
	- Em uma rede centralizada ha um node central que recebe as informacoes dos nodes e replica as informacoes para os outros nodes da rede.<br>
	- Quando o dado move de um node para o outro é chamado de propragacao.<br>
	- Leva tempo para propragar o mesmo dado para todos os nodes da rede.<br>
	- Latencia é o termo usado para indicar a demora para a propragacao do dado.<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=22)
</details>

**Ledger:**
<details><summary>Click to expand</summary>
	- Ledger é uma base de dados que registra todas as transacoes confirmadas/validas.<br>
	- Cada nó tem a copia da ledger.<br>
	- Blockchain do Bitcoin apenas armazena na ledger informacoes das transacoes de bitcoin, blockchains como Ethereum podem armazenar qualquer tipo de informacoes.<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=24)
</details>

**Blocks and Miners:**
<details><summary>Click to expand</summary>
	- Uma blockchain ledger é um conjuto de blocos que estao conectados uns com os outros.<br>
	- Cada block tem um Header que contem o hash do bloco anterior, merkle root hash e Nonce que é o resultado do puzzle do bloco e a lista de transações.<br>
	- Cada transacao tem os seguintes campos: Hash da transacao, Numero do bloco, Timestamp, Endereço de origem, Endereço destino, Valor, Taxa, Input(dados). 
	- Os blocos estao conectados uns com os outros pela informacao do header que tem o hash do bloco anterior.<br>
	- O primeiro bloco da cadeia é chamado de genesis block, sendo o bloco numero zero. Os blocos menores que nao enviados a cadeia de blocos pricipais sao chamados de orhphaned forks.<br>
	- Nodes especiais estao criando esses nodes na Peer-to-peer network, esses nodes sao chamados de miners/mineradores.<br>
	- Todos os mineradores estao coletados todas as transações realizads na rede, mas apenas as transações validas sao repassadas aos outros nodes. Cada minerador pega um numero dessas transacoes e coloca um novo bloco, essas listas de transacoes sao numeradas por tx0, tx1, tx2 etc...<br>
	- Cada transacao há uma taxa, e na blockchain do Bitcoin a taxa do transacao fica com o minerador que validou essa transacao(isso se o bloco for para a rede principal).<br>
	- Quando um minerador cria um bloco ele tem que resolver um puzzle(computacional) aplicado a lista de transacoes.<br>
	- O mineradorque resolver o puzzle primeiro é permitido a colocar o bloco na rede principal.<br>
	- O bloco contem tambem a resolucao do puzzle, que vai no campo nonce, que esta no header do bloco.<br>
	- Os outros mineradores vao receber esse bloco e validar se esta ok antes de colocar na cadeia de blocos principais.<br>
	- Quando dois blocos(com as mesma transacoes) sao resolvidos ao mesmo tempo é criado um fork, e a cadeia principal segue com o lado em for mais longo(antigo) em relacao com o block genesis. Os blocos que nao seguem para a cadeia de blocos principais sao chamados de orfãs.<br>
	[Youtube Video](https://www.youtube.com/watch?v=OFOLm1g1sWI&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=25)
</details>

**Testnet and faucets:**
<details><summary>Click to expand</summary>
	- Testnet is a alternative blockchain used to developers test their code.<br>
	- Coins on testnet doesn't have value.<br>
	- The main chain is called mainnet.<br>
	- You can easily mining BTC and ETH coins on testnet, the hash on testnet is low enough to you can solve the puzzle.<br>
	- Adress on Bitcoin mainet and testnet are different, on testnet adress begins with m/n, on Ethereum both chain have the same adress.<br>
	- Faucet is a website that gives to you a small amount of a coin when you do some task.
	[Youtube Video](https://www.youtube.com/watch?v=gIZCQUatkes&list=PLmL13yqb6OxdEgSoua2WuqHKBuIqvll0x&index=26)
</details>

**Wallets:**
<details><summary>Click to expand</summary>
	- Wallet stores private key, and the public key that is derivated from private key.<br>
	- Wallet not store your coins, coins are stored on blockchain, wallet just get the keys to search on blockchain and return the balance.<br>
	- Wallet store keys not coins, if you lose your wallet but still have your private keys you are still able to manage your assests.<br>
	- Wallet store private keys but also create those private keys.<br>
	- The public key is used to deposit cryptos into wallet and private key is used to withdraw.<br>
	- Carteiras determinísticas: Contém as chaves privadas derivadas de uma semente - seed - podendo gerar inúmeros endereços.
	- Existem atualmente três tipos de endereços no Bitcoin Core:
	<details><summary>Click to expand</summary>
	  • P2PKH
	  • P2SH
	  • bech32
	</details>
</details>

**Como Garantir a Origem dos Eventos na Blockchain?**
<details><summary>Click to expand</summary>
    - Como é Verificado a Autenticidade da transação?: O usuário assina a transação com sua chave privada. É feito a validação do conteudo. A rede faz a verificação da assinatura, usando a publick key do usuario, vendo se chega a na private key que foi usado na assinatura(asymmetric Encryption), se bater é valido, s enão falha, tentativa de fraude.
</details>
